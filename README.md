# Entanglement Pre-Release [![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/powered-by-black-magic.svg)](https://forthebadge.com)

### Status

| Branch      | Build Status |
| ----------- | ----------- |
| main | ![Main Pipeline](https://gitlab.com/zCubed3/entanglement-public/badges/main/pipeline.svg) |
| zcubed-staging | ![zCubed Pipeline](https://gitlab.com/zCubed3/entanglement-public/badges/zcubed-staging/pipeline.svg) |
| laka-staging | ![Lakatrazz Pipeline](https://gitlab.com/zCubed3/entanglement-public/badges/laka-staging/pipeline.svg) |

## Changelog
The changelog is part of a separate document since we append it to the bottom of the README in the thunderstore release + it helps keep track of dev changes!

#### [CHANGELOG.md](CHANGELOG.md)

## Disclaimer

#### THIS PROJECT IS NOT AFFILIATED WITH THE ORIGINAL "boneworks-mp" PROJECT! It is its own standalone mod and shares no code with the original mod, any similarities are coincidental!
