﻿using UnityEngine;

using StressLevelZero.Pool;
using StressLevelZero.Props.Weapons;
using StressLevelZero.Data;

using HarmonyLib;

using Entanglement.Network;
using Entanglement.Data;
using Entanglement.Objects;

using System.Collections;

using MelonLoader;

namespace Entanglement.Patching
{
    [HarmonyPatch(typeof(SpawnGun), "OnFire")]
    public class SpawnFirePatch
    {
        private static int spawnCount = 0;

        public static void Postfix(SpawnGun __instance) {
            // Prevents spawning twice
            if (spawnCount <= 0) { 
                spawnCount++; return;
            }
            spawnCount = 0;

            if (!DiscordIntegration.hasLobby)
                return;

            MelonCoroutines.Start(SpawnGunFire(__instance));
        }

        public static IEnumerator SpawnGunFire(SpawnGun __instance)
        {
            SpawnableObject spawnable = __instance._selectedSpawnable;

            if (__instance._selectedMode != UtilityModes.SPAWNER || !spawnable)
                yield break;

            yield return null;

            yield return null;

            Pool objPool = PoolManager.GetPool(spawnable.title);

            if (!objPool)
                yield break;

            Poolee lastSpawn = objPool._lastSpawn;
            if (!lastSpawn)
                yield break;

            Transform objTransform = lastSpawn.transform;
            Vector3 position = objTransform.position;
            Quaternion rotation = objTransform.rotation;

            long userId = DiscordIntegration.currentUser.Id;

            ushort? objectId = null;
            ushort callbackIndex = 0;

            if (Server.instance != null) {
                objectId = ObjectSync.lastId;
                objectId++;
                ObjectSync.lastId = objectId.Value;

#if DEBUG
                EntangleLogger.Log($"Creating host spawn object id {objectId}.");
#endif
            }

            Syncable syncable = SyncedTransform.CreateSync(userId, lastSpawn.gameObject.GetComponent<Rigidbody>(), objectId);

            if (Server.instance == null) {
                if (syncable != null) {
                    callbackIndex = ObjectSync.QueueSyncable(syncable);
                    syncable.isValid = false;
                }
                objectId = 0;
            }

            SpawnMessageData spawnData = new SpawnMessageData()
            {
                userId = userId,
                objectId = objectId.Value,
                callbackIndex = callbackIndex,
                spawnableTitle = spawnable.title,
                spawnTransform = new SimplifiedTransform(position, rotation)
            };

            NetworkMessage message = NetworkMessage.CreateMessage((byte)BuiltInMessageType.SpawnObject, spawnData);
            Node.activeNode.BroadcastMessage(NetworkChannel.Object, message.GetBytes());
        }
    }
}
