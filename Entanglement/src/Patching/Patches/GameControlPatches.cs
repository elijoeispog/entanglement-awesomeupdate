﻿using HarmonyLib;

using System;

using Entanglement.Network;

namespace Entanglement.Patching {
    // This patch removes annoying scene reloads which break Entanglement until level reload
    [HarmonyPatch(typeof(GameControl), "RELOADLEVEL")]
    public static class ReloadLevelPatch {
        public static void Prefix() {
            if (DiscordIntegration.hasLobby)
                throw new Exception("PlayerTrigger has entered a Reload Volume or died during an Entanglement session.");
        }
    }
}
